package tech.ivoice.util;

import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.json.JSONArray;

import java.io.IOException;

public abstract class HttpUtil {
    public static String getAnswer(String question) {
        try {
            Content postResult = Request.Post("https://odqa.demos.ivoice.online/model")
                    .bodyString("{ \"context\": [ \"" + question + "\" ]}", ContentType.APPLICATION_JSON)
                    .execute().returnContent();

            JSONArray jsonArray = new JSONArray(postResult.asString());

            String answer = jsonArray
                    .getJSONArray(0)
                    .getJSONArray(0)
                    .getString(0);

            if (answer != null) {
                return answer;
            } else {
                return "Please, try again.";
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "Please, try again.";
        }
    }
}
