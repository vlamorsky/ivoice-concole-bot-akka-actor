package tech.ivoice.util;

import java.util.Scanner;

public abstract class InputUtil {
    public static String readQuestion() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("please, ask your question");
        String userQuestion = scanner.nextLine();

        return (userQuestion != null) ? userQuestion : "here will be a question";
    }

    public static boolean needNextQuestion() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ask next question? [y/n]");
        String userInput = scanner.nextLine();

        if (userInput.length() == 1 &&
                (userInput.charAt(0) == 'n' || userInput.charAt(0) == 'N')) {
            return false;
        }

        return true;
    }
}
