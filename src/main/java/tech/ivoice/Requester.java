package tech.ivoice;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import tech.ivoice.util.InputUtil;

public class Requester extends AbstractBehavior<Requester.GetRequest> {

    public static class GetRequest {
        private ActorRef<MainActor.Command> mainActor;

        public GetRequest(ActorRef<MainActor.Command> mainActor) {
            this.mainActor = mainActor;
        }
    }

    public static Behavior<Requester.GetRequest> create() {
        return Behaviors.setup(Requester::new);
    }

    private Requester(ActorContext<Requester.GetRequest> context) {
        super(context);
    }

    @Override
    public Receive<GetRequest> createReceive() {
        return newReceiveBuilder()
                .onMessage(GetRequest.class, this::onGetRequest)
                .build();
    }

    private Behavior<GetRequest> onGetRequest(GetRequest message) {
        if (InputUtil.needNextQuestion()) {
            message.mainActor.tell(MainActor.NewQuestion.INSTANCE);
        } else {
            message.mainActor.tell(MainActor.Stop.INSTANCE);
        }

        return this;
    }


}
