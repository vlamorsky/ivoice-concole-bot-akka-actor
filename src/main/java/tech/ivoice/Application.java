package tech.ivoice;

import akka.actor.typed.ActorSystem;

public class Application {

    public static void main(String[] args) {
        ActorSystem<MainActor.Command> actorSystem = ActorSystem.create(MainActor.create(), "actorSystem");

        actorSystem.tell(MainActor.Start.INSTANCE);
    }
}
