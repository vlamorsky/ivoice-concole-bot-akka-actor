package tech.ivoice;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import tech.ivoice.util.HttpUtil;

public class Answerer extends AbstractBehavior<Answerer.GiveAnswer> {

    public static class GiveAnswer {
        private final String question;
        private ActorRef<MainActor.Command> mainActor;

        public GiveAnswer(String question, ActorRef<MainActor.Command> mainActor) {
            this.question = question;
            this.mainActor = mainActor;
        }
    }

    public static Behavior<Answerer.GiveAnswer> create() {
        return Behaviors.setup(Answerer::new);
    }

    private final ActorRef<Requester.GetRequest> requester;

    private Answerer(ActorContext<Answerer.GiveAnswer> context) {
        super(context);
        requester = getContext().spawn(Requester.create(), "requester");
    }

    @Override
    public Receive<GiveAnswer> createReceive() {
        return newReceiveBuilder()
                .onMessage(GiveAnswer.class, this::onGiveAnswer)
                .build();
    }

    private Behavior<GiveAnswer> onGiveAnswer(GiveAnswer message) {
        String answer = HttpUtil.getAnswer(message.question);

        System.out.println(answer);

        requester.tell(new Requester.GetRequest(message.mainActor));
        return this;
    }


}
