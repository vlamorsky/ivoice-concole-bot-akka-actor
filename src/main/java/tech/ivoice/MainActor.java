package tech.ivoice;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;

public class MainActor extends AbstractBehavior<MainActor.Command> {

    interface Command {}

    public enum Start implements Command {
        INSTANCE
    }

    public enum Stop implements Command {
        INSTANCE
    }

    public enum NewQuestion implements Command {
        INSTANCE
    }

    private ActorRef<Questioner.GetQuestion> questioner;

    public static Behavior<Command> create() {
        return Behaviors.setup(MainActor::new);
    }

    private MainActor(ActorContext<Command> context) {
        super(context);
    }

    @Override
    public Receive<Command> createReceive() {
        return newReceiveBuilder()
                .onMessageEquals(Start.INSTANCE, this::onStart)
                .onMessageEquals(Stop.INSTANCE, this::onStop)
                .onMessageEquals(NewQuestion.INSTANCE, this::onNewQuestion)
                .build();
    }

    private Behavior<Command> onStart() {
        questioner = getContext().spawn(Questioner.create(), "questioner");
        questioner.tell(new Questioner.GetQuestion(getContext().getSelf()));
        return this;
    }

    private Behavior<Command> onStop() {
        getContext().getSystem().terminate();
        return this;
    }

    private Behavior<Command> onNewQuestion() {
        questioner.tell(new Questioner.GetQuestion(getContext().getSelf()));
        return this;
    }
}
