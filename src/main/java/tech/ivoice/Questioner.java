package tech.ivoice;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import tech.ivoice.util.InputUtil;

public class Questioner extends AbstractBehavior<Questioner.GetQuestion> {

    public static class GetQuestion {
        private ActorRef<MainActor.Command> mainActor;

        public GetQuestion(ActorRef<MainActor.Command> mainActor) {
            this.mainActor = mainActor;
        }
    }

    public static Behavior<GetQuestion> create() {
        return Behaviors.setup(Questioner::new);
    }

    private final ActorRef<Answerer.GiveAnswer> answerer;

    private Questioner(ActorContext<GetQuestion> context) {
        super(context);
        answerer = getContext().spawn(Answerer.create(), "answerer");
    }

    @Override
    public Receive<GetQuestion> createReceive() {
        return newReceiveBuilder()
                .onMessage(GetQuestion.class, this::onGetQuestion)
                .build();
    }

    private Behavior<GetQuestion> onGetQuestion(GetQuestion message) {
        String question = InputUtil.readQuestion();

        answerer.tell(new Answerer.GiveAnswer(question, message.mainActor));
        return this;
    }


}
