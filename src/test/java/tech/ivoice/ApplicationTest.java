package tech.ivoice;

import akka.actor.testkit.typed.javadsl.TestKitJunitResource;
import akka.actor.testkit.typed.javadsl.TestProbe;
import akka.actor.typed.ActorRef;
import org.junit.Test;

import java.io.ByteArrayInputStream;

public class ApplicationTest {

    @Test
    public void MainActor_should_receive_newQuestion_message() {
        TestKitJunitResource testKit = new TestKitJunitResource();

        System.setIn(new ByteArrayInputStream("y\n".getBytes()));

        TestProbe<MainActor.Command> testProbe = testKit.createTestProbe();
        ActorRef<Requester.GetRequest> testRef = testKit.spawn(Requester.create(), "requester");
        testRef.tell(new Requester.GetRequest(testProbe.getRef()));

        testProbe.expectMessage(MainActor.NewQuestion.INSTANCE);
    }

    @Test
    public void MainActor_should_terminate() {
        TestKitJunitResource testKit = new TestKitJunitResource();

        System.setIn(new ByteArrayInputStream("n\n".getBytes()));

        TestProbe<MainActor.Command> testProbe = testKit.createTestProbe();
        ActorRef<Requester.GetRequest> testRef = testKit.spawn(Requester.create());
        testRef.tell(new Requester.GetRequest(testProbe.getRef()));

        testProbe.stop();
    }
}
