package tech.ivoice.util;

import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class InputUtilTest {

    @Test
    public void readQuestion_should_return_question() {

        String expected = "How are you?";
        System.setIn(new ByteArrayInputStream(expected.getBytes()));
        Assert.assertEquals(expected, InputUtil.readQuestion());
    }

    @Test
    public void readQuestion_should_print_ask_your_question() {

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);

        System.setOut(printStream);
        System.setIn(new ByteArrayInputStream("question".getBytes()));
        InputUtil.readQuestion();

        Assert.assertEquals("please, ask your question", new Scanner(outputStream.toString()).nextLine());
    }

    @Test
    public void needNextQuestion_should_return_false_for_input_n() {

        System.setIn(new ByteArrayInputStream("n".getBytes()));
        Assert.assertEquals(false, InputUtil.needNextQuestion());
    }

    @Test
    public void needNextQuestion_should_return_true_for_input_y() {

        System.setIn(new ByteArrayInputStream("y".getBytes()));
        Assert.assertEquals(true, InputUtil.needNextQuestion());
    }

    @Test
    public void needNextQuestion_should_print_ask_next_question() {

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);

        System.setOut(printStream);
        System.setIn(new ByteArrayInputStream("question".getBytes()));
        InputUtil.needNextQuestion();

        Assert.assertEquals("Ask next question? [y/n]", new Scanner(outputStream.toString()).nextLine());
    }
}
