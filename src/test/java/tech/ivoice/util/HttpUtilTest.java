package tech.ivoice.util;

import org.junit.Assert;
import org.junit.Test;

public class HttpUtilTest {

    @Test
    public void getAnswer_for_question_help() {

        String expected = "If you have any further inquiries, you can address them to the International Students Office, " +
                "which is located in the Auditorium Building, Room 315. The phone number is (7-495) 408-7043.";

        Assert.assertEquals(expected, HttpUtil.getAnswer("help"));

    }

}
